require 'spec_helper'

describe V1::SourcesController do

  describe 'routing' do
    let(:id) { Fabricate(:object_id).to_s }

    it 'routes to #create' do
      post('/1/sources').should route_to('v1/sources#create')
    end

    it 'routes to #index' do
      get('/1/sources').should route_to('v1/sources#index')
    end

  end
end




