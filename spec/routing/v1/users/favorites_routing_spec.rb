require 'spec_helper'

describe V1::Users::FavoritesController do

  describe 'routing' do
    let(:user_id) { Fabricate(:object_id).to_param }

    it 'routes to #index' do
      get("/1/users/#{user_id}/favorites").should route_to('v1/users/favorites#index', user_id: user_id)
    end

    it 'routes to #create' do
      post("/1/users/#{user_id}/favorites").should route_to('v1/users/favorites#create', user_id: user_id)
    end

  end
end






