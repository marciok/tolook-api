require 'spec_helper'

describe V1::LooksController do

  describe 'routing' do
    let(:id) { Fabricate(:object_id).to_s }

    it 'routes to #index' do
      get('/1/looks').should route_to('v1/looks#index')
    end

    it 'routes to #show' do
      get("/1/looks/#{id}").should route_to('v1/looks#show', id: id)
    end

    it 'routes to #create' do
      post('/1/looks').should route_to('v1/looks#create')
    end

  end
end



