require 'spec_helper'

describe 'v1/users/show', formats: [ :json ] do
  let!(:user) { assign(:user, Fabricate(:user)) }

  describe 'User API JSON representation' do
    before { render }

    let(:json) { ActiveSupport::JSON.decode(rendered) }

    subject { json }

    it { should include('user') }

    describe 'user' do
      subject { json['user'] }

      describe 'user' do
        let(:user_json) { json['user'] }

        subject { user_json }

        it { should include('id') }

        describe 'id' do
          subject { user_json['id'] }

          it { should eql(user.id.to_s) }
        end

      end

    end

  end

end




