require 'spec_helper'

describe 'v1/users/favorites/index', formats: [ :json ] do
  let!(:favorites) { assign(:favorites, [ Fabricate(:favorite) ]) }

  describe 'User API JSON representation' do
    before { render }

    let(:json) { ActiveSupport::JSON.decode(rendered) }

    subject { json }

    it { should include('favorites') }

    describe 'favorites' do
      subject { json['favorites'] }

      it { should be_an(Array) }

      describe 'favorite' do
        let(:favorite) { favorites.first }
        let(:favorite_json) { json['favorites'].first }

        subject { favorite_json }

        it { should include('id') }

        describe 'id' do
          subject { favorite_json['id'] }

          it { should eql(favorite.id.to_s) }
        end

        it { should include('thumb') }

        describe 'thumb' do
          subject { favorite_json['thumb'] }

          it { should eql(favorite.thumb) }
        end

      end

    end

  end

end




