require 'spec_helper'

describe 'v1/looks/show', formats: [ :json ] do  
  let(:garments) { [ Fabricate(:garment) ] }
  let(:occasions) { [ Fabricate(:occasion) ] }
  let!(:look) { assign(:look, Fabricate(:look, garments: garments, occasions: occasions)) }

  describe 'Looks API JSON representation' do
    before { render }

    let(:json) { ActiveSupport::JSON.decode(rendered) }

    subject { json }

    it { should include('look') }

    describe 'look' do
      subject { json['look'] }

        it { should include('id') }

        describe 'id' do
          subject { json['look']['id'] }

          it { should eql(look.id.to_s) }

        end

        it { should include('picture') }

        describe 'picture' do
          subject { json['look']['picture'] }

          it { should eql(look.picture) }

        end

        it { should include('garments') }

        describe 'garments' do
          subject { json['look']['garments'] }

          it { should be_an(Array) }

          describe 'garment' do
            let(:garment) { garments.first }
            let(:garment_json) { json['look']['garments'].first }

            subject { garment_json }

            it { should include('id') }

            describe 'id' do
              subject { garment_json['id'] }

              it { should eql(garment.id.to_s) }
            end

            it { should include('name') }

            describe 'name' do
              subject { garment_json['name'] }

              it { should eql(garment.name) }
            end
          end

        end

        it { should include('occasions') }

        describe 'occasions' do
          subject { json['look']['occasions'] }

          it { should be_an(Array) }

          describe 'occasion' do
            let(:occasion) { occasions.first }
            let(:occasion_json) { json['look']['occasions'].first }

            subject { occasion_json }

            it { should include('id') }

            describe 'id' do
              subject { occasion_json['id'] }

              it { should eql(occasion.id.to_s) }
            end

            it { should include('name') }

            describe 'name' do
              subject { occasion_json['name'] }

              it { should eql(occasion.name) }
            end
          end

        end

      it { should include('reference') }

      describe 'reference' do
        subject { json['look']['reference'] }

        it { should eql(look.reference) }

      end

    end

  end

end


