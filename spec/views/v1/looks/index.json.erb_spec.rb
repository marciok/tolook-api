require 'spec_helper'

describe 'v1/looks/index', formats: [ :json ] do  
  let!(:looks) { assign(:looks, [ Fabricate(:look) ]) }

  describe 'Looks API JSON representation' do
    before { render }

    let(:json) { ActiveSupport::JSON.decode(rendered) }

    subject { json }

    it { should include('looks') }

    describe 'looks' do
      subject { json['looks'] }

      it { should be_an(Array) }

      describe 'look' do
        let(:look) { looks.first }
        let(:look_json) { json['looks'].first }

        subject { look_json }

        it { should include('id') }

        describe 'id' do
          subject { look_json['id'] }

          it { should eql(look.id.to_s) }
        end

        it { should include('picture') }

        describe 'picture' do
          subject { look_json['picture'] }

          it { should eql(look.picture) }
        end

        it { should include('created_at') }

        describe 'created_at' do
          subject { look_json['created_at'] }

          it { should eql(look.created_at.strftime('%H:%M')) }
        end

        it { should include('source') }

        describe 'source' do
          let(:source) { look.source }
          let(:source_json) { look_json['source'] }

          subject { source_json }

          it { should include('id') }

          describe 'id' do
            subject { source_json['id'] }

            it { should eql(source.id.to_s) }
          end

          it { should include('name') }

          describe 'name' do
            subject { source_json['name'] }

            it { should eql(source.name) }
          end

          it { should include('thumb') }

          describe 'thumb' do
            subject { source_json['thumb'] }

            it { should eql(source.thumb) }
          end

        end

      end

    end

  end

end

