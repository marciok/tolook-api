require 'spec_helper'

describe 'v1/categories/show', formats: [ :json ] do  
  let(:sources) { [ Fabricate(:source) ] }
  let!(:category) { assign(:category, Fabricate(:category, sources: sources)) }

  describe 'Categorie API JSON representation' do
    before { render }

    let(:json) { ActiveSupport::JSON.decode(rendered) }

    subject { json }

    it { should include('category') }

    describe 'category' do
      subject { json['category'] }

      it { should include('name') }

      describe 'name' do
        subject { json['category']['name'] }

        it { should eql(category.name) }
      end

      it { should include('sources') }

      describe 'sources' do
        subject { json['category']['sources'] }

        it { should be_an(Array) }

        describe 'source' do
          let(:source) { sources.first }
          let(:source_json) { json['category']['sources'].first }

          subject { source_json }

          it { should include('id') }

          describe 'id' do
            subject { source_json['id'] }

            it { should eql(source.id.to_s) }
          end

          it { should include('name') }

          describe 'name' do
            subject { source_json['name'] }

            it { should eql(source.name) }
          end

          it { should include('thumb') }

          describe 'name' do
            subject { source_json['thumb'] }

            it { should eql(source.thumb) }
          end

          it { should include('entries') }

          describe 'entries' do
            subject { source_json['entries'] }

            it { should eql(source.entries) }
          end

        end

      end

    end

  end

end




