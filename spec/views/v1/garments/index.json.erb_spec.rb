require 'spec_helper'

describe 'v1/garments/index', formats: [ :json ] do  
  let!(:garments) { assign(:garments, [ Fabricate(:garment) ]) }

  describe 'Garments API JSON representation' do
    before { render }

    let(:json) { ActiveSupport::JSON.decode(rendered) }

    subject { json }

    it { should include('garments') }

    describe 'garments' do
      subject { json['garments'] }

      it { should be_an(Array) }

      describe 'garment' do
        let(:garment) { garments.first }
        let(:garment_json) { json['garments'].first }

        subject { garment_json }

        it { should include('id') }

        describe 'id' do
          subject { garment_json['id'] }

          it { should eql(garment.id.to_s) }
        end

        it { should include('name') }

        describe 'name' do
          subject { garment_json['name'] }

          it { should eql(garment.name) }
        end

        it { should include('thumb') }

        describe 'thumb' do
          subject { garment_json['thumb'] }

          it { should eql(garment.thumb) }
        end

        it { should include('thumb') }

        describe 'entries' do
          subject { garment_json['entries'] }

          it { should eql(garment.entries) }
        end

      end

    end

  end

end



