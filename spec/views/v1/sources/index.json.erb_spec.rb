require 'spec_helper'

describe 'v1/sources/index', formats: [ :json ] do
  let(:url) { Fabricate(:url) }
  let!(:sources) { assign(:sources, [ url.source ]) }

  describe 'Sources API JSON representation' do
    before { render }

    let(:json) { ActiveSupport::JSON.decode(rendered) }

    subject { json }

    it { should include('sources') }

    describe 'sources' do
      subject { json['sources'] }

      it { should be_an(Array) }

      describe 'source' do
        let(:source) { sources.first }
        let(:source_json) { json['sources'].first }

        subject { source_json }

        it { should include('id') }

        describe 'id' do
          subject { source_json['id'] }

          it { should eql(source.id.to_s) }
        end

        it { should include('title') }

        describe 'title' do
          subject { source_json['title'] }

          it { should eql(source.title) }
        end

        it { should include('thumb') }

        describe 'thumb' do
          subject { source_json['thumb'] }

          it { should eql(source.thumb) }
        end

        it { should include('entries') }

        describe 'entries' do
          subject { source_json['entries'] }

          it { should eql(source.entries) }
        end

      end

    end

  end

end


