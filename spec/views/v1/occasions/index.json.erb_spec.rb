require 'spec_helper'

describe 'v1/occasions/index', formats: [ :json ] do  
  let!(:occasions) { assign(:occasions, [ Fabricate(:occasion) ]) }

  describe 'Occasions API JSON representation' do
    before { render }

    let(:json) { ActiveSupport::JSON.decode(rendered) }

    subject { json }

    it { should include('occasions') }

    describe 'occasions' do
      subject { json['occasions'] }

      it { should be_an(Array) }

      describe 'occasion' do
        let(:occasion) { occasions.first }
        let(:occasion_json) { json['occasions'].first }

        subject { occasion_json }

        it { should include('id') }

        describe 'id' do
          subject { occasion_json['id'] }

          it { should eql(occasion.id.to_s) }
        end

        it { should include('name') }

        describe 'name' do
          subject { occasion_json['name'] }

          it { should eql(occasion.name) }
        end

        it { should include('thumb') }

        describe 'thumb' do
          subject { occasion_json['thumb'] }

          it { should eql(occasion.thumb) }
        end

        it { should include('thumb') }

        describe 'entries' do
          subject { occasion_json['entries'] }

          it { should eql(occasion.entries) }
        end

      end

    end

  end

end



