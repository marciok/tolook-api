require 'spec_helper'

describe Category do
  it { should be_a_kind_of(Mongoid::Document)}

  describe 'Associations' do
    it { should have_and_belong_to_many(:sources) }
  end

  describe 'Attributes' do
    it { should have_field(:name).of_type(String) }
  end

  describe 'Validations' do
    it { should validate_presence_of(:name) }
  end

end
