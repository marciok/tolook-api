require 'spec_helper'

describe Source do
  it { should be_a_kind_of(Mongoid::Document)}

  describe 'Associations' do
    it { should have_and_belong_to_many(:categories) }
    it { should have_many(:looks) }
    it { should embed_one(:url) }
  end

  describe 'Attributes' do
    it { should have_field(:name).of_type(String) }
    it { should have_field(:title).of_type(String) }
    it { should have_field(:thumb).of_type(String) }
    it { should have_field(:entries).of_type(Integer).with_default_value_of(0) }
  end

  describe 'Validation' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:thumb) }
    it { should validate_numericality_of(:entries).to_allow(only_integer: true, greater_than_or_equal_to: 0) }
  end
end
