require 'spec_helper'

describe User do
  it { should be_a_kind_of(Mongoid::Document) }

  describe 'Associations' do
    it { should have_and_belong_to_many(:favorites).of_type(Look).as_inverse_of(nil) }
  end

end

