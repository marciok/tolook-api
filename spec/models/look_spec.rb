require 'spec_helper'

describe Look do
  it { should be_a_kind_of(Mongoid::Document) }

  describe 'Associations' do
    it { should belong_to(:source) }
    it { should have_and_belong_to_many(:occasions) }
    it { should have_and_belong_to_many(:garments) }
  end

  describe 'Attributes' do
    it { should have_field(:picture).of_type(String) }
    it { should have_field(:thumb).of_type(String) }
    it { should have_field(:reference).of_type(String) }
  end

  describe 'Validation' do
    it { should validate_presence_of(:picture) }
    it { should validate_presence_of(:thumb) }
    it { should validate_presence_of(:reference) }
    it { should validate_uniqueness_of(:picture) }
  end
end
