require 'spec_helper'

describe Url do
  it { should be_a_kind_of(Mongoid::Document) }

  describe 'Associations' do
    it { should be_embedded_in(:source) }
  end

  describe 'Attributes' do
    it { should have_field(:site).of_type(String) }
    it { should have_field(:feed).of_type(String) }
  end

  describe 'Validation' do
    it { should validate_presence_of(:site) }
    it { should validate_presence_of(:feed) }
  end
end


