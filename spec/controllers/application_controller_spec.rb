require 'spec_helper'

describe ApplicationController do

  context 'private methods' do
 
    describe '#return_not_found' do
      let(:params) { { format: :json } }
      let(:test_request) { get :index, params }
	
  	  controller do
  	    def index
  	      return_not_found
  	    end
  	  end
	
      before { test_request }

      it 'returns HTTP Not Found' do
        response.code.should eq('404')
      end
    end
 
  end
 
end


