require 'spec_helper'

describe V1::OccasionsController do
  it { should be_a_kind_of(V1Controller) }

  describe 'GET index' do
    let(:test_request) { get :index, format: :json }

    before { test_request }

    let(:occasions) { [ Fabricate(:occasion) ] }

    it 'assigns the Occasion collection as @occasions' do
      assigns(:occasions).should eq(occasions)
    end

    it 'renders the Occasion collection API representation' do
      response.should render_template(:index)
    end

    it 'returns HTTP OK' do
      response.code.should eq('200')
    end
  end

end

