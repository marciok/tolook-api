require 'spec_helper'

describe V1::LooksController do
  it { should be_a_kind_of(V1Controller) }

  describe 'GET index' do
    let(:test_request) { get :index, format: :json }

    before { test_request }

    let(:looks) { [ Fabricate(:look) ] }

    it 'assigns the Look collection as @looks' do
      assigns(:looks).should eq(looks)
    end

    it 'renders the Look collection API representation' do
      response.should render_template(:index)
    end

    it 'returns HTTP OK' do
      response.code.should eq('200')
    end
  end

  describe 'GET show' do
    let(:test_request) { get :show, id: look_id, format: :json }

    before { test_request }

    context 'there is an Look with the ID supplied' do
      let(:look) { Fabricate(:look) }
      let(:look_id) { look.to_param }

      it 'assigns the requested Look as @look' do
        assigns(:look).should eq(look)
      end

      it 'renders the Look API representation' do
        response.should render_template(:show)
      end

      it 'returns HTTP OK' do
        response.code.should eq('200')
      end
    end

    context 'there is not an Look with the ID supplied' do
      let(:look_id) { Fabricate(:object_id).to_param }

      it 'returns HTTP Not Found' do
        response.code.should eq('404')
      end
    end
  end


end
