require 'spec_helper'

describe V1::UsersController do
  it { should be_a_kind_of(V1Controller) }

  describe 'POST create' do
    let(:test_request) { post :create, format: :json }

    it 'creates a new User' do
      expect { test_request }.to change(User, :count).by(1)
    end

    describe 'post-conditions' do
      before { test_request }

      it 'assigns the newly created User as @user' do
        assigns(:user).should be_an(User)
      end

      it 'persists the newly created User' do
        assigns(:user).should be_persisted
      end

      it 'renders the User API representation' do
        response.should render_template(:show)
      end

      it 'returns HTTP Created' do
        response.code.should eq('201')
      end
    end
  end

end

