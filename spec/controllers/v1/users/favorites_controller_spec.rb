require 'spec_helper'

describe V1::Users::FavoritesController do
  it { should be_a_kind_of(V1::UsersController) }

  describe 'GET index' do
    let(:user) { Fabricate(:user) }
    let(:favorites) { user.favorites }
    let(:user_id) { user.to_param }
    let(:test_request) { get :index, user_id: user_id, format: :json }

    before { test_request }

    it 'assigns the Favorite collection as @favorites' do
      assigns(:favorites).should eq(favorites)
    end

    it 'renders the Favorite collection API representation' do
      response.should render_template(:index)
    end

    it 'returns HTTP OK' do
      response.code.should eq('200')
    end
  end

  describe 'POST create' do
    let(:user) { Fabricate(:user) }
    let(:user_id) { user.to_param }
    let(:test_request) { post :create, user_id: user_id, format: :json }

    before { test_request }

    # it 'creates a new Favorite' do
    #   expect { test_request }.to change(user.favorites, :count).by(1)
    # end

  #   describe 'post-conditions' do
  #     before { test_request }

  #     it 'assigns the newly created Favorite as @favorite' do
  #       assigns(:favorite).should be_an(Favorite)
  #     end

  #     it 'persists the newly created Favorite' do
  #       assigns(:favorite).should be_persisted
  #     end

  #     it 'renders the Favorite API representation' do
  #       response.should render_template(:show)
  #     end

  #     it 'returns HTTP Created' do
  #       response.code.should eq('201')
  #     end
  #   end
  end

end




