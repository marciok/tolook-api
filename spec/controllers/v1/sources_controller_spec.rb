require 'spec_helper'

describe V1::SourcesController do
  it { should be_a_kind_of(V1Controller) }

  describe 'GET index' do
    let(:test_request) { get :index, format: :json }
    let(:sources) { [ Fabricate(:source) ] }

    before do
      test_request
      Source.stub_chain(:all, :order_by).and_return(sources)
    end


    it 'assigns the Source collection as @sources' do
      assigns(:sources).should eq(sources)
    end

    it 'renders the Source collection API representation' do
      response.should render_template(:index)
    end

    it 'returns HTTP OK' do
      response.code.should eq('200')
    end
  end

end
