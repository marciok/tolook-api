require 'spec_helper'


describe V1::GarmentsController do
  it { should be_a_kind_of(V1Controller) }
  
  describe 'GET index' do
    let(:test_request) { get :index, format: :json }

    before { test_request }

    let(:garments) { [ Fabricate(:garment) ] }

    it 'assigns the Garment collection as @garments' do
      assigns(:garments).should eq(garments)
    end

    it 'renders the Garment collection API representation' do
      response.should render_template(:index)
    end

    it 'returns HTTP OK' do
      response.code.should eq('200')
    end

  end

end
