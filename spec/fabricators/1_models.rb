Fabricator(:user) do
  favorites(count: 3) { Fabricate(:favorite) }
end

Fabricator(:favorite, class_name: Look) do
  picture { Faker::Internet.url }
  thumb { Faker::Internet.url }
  reference { Faker::Internet.url }
end

Fabricator(:look) do
  picture { Faker::Internet.url }
  thumb { Faker::Internet.url }
  reference { Faker::Internet.url }
  source
end

Fabricator(:category) do
  name { Faker::Name.name }
end

Fabricator(:source) do
  name { Faker::Name.name }
  title { Faker::Name.name }
  thumb { Faker::Internet.url }
  entries { Random.rand(0..5000) }
end

Fabricator(:url) do
  site { Faker::Internet.url }
  feed { Faker::Internet.url }
  source
end

Fabricator(:garment) do
  name { Faker::Name.name }
  thumb { Faker::Internet.url }
end

Fabricator(:occasion) do
  name { Faker::Name.name }
  thumb { Faker::Internet.url }
end
