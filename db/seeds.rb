# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# garments = ['Dresses', 'Blouses', 'T-Shirts' 'Sweaters/ Cardigans', 'Jackets/ Blazers', 'Skirts', 'Shorts', 'Pants']
#

puts '*************************'
# Seed Garments
garments = [
 'Dresses',
 'Blouses',
 'T-Shirts',
 'Sweaters / Cardigans',
 'Jackets / Blazers', 
 'Skirts',
 'Shorts',
 'Pants',
 'Denim',
 'Print',
 'Animal Print',
 'Boot',
 'Stripes',
 'Leather',
 'Fur',
 'Tank top',
 'Tricot',
 'Check',
 'Ton Sur Ton',
 'Floral',
 'Bikini',
 'Jumpsuit',
 'Vest',
 'Lace'
]

garments.collect do |garment|
  Garment.create!({ name: garment, thumb: 'http://fakeimg.pl/30/' })
  puts "Garment -> #{garment}"
end

puts '*************************'
# Seed Occasions
occasions = [
 'Casual Day Out',
 'Casual Night Out',
 'Beachwear',
 'Sportwear',
 'Black-tie',
 'Work',
 'Day Party',
 'Evening Party'
]

occasions.collect do |occasion|
  Occasion.create!({
    name: occasion,
    thumb: 'http://fakeimg.pl/30/'
  })
  puts "Occasion -> #{occasion}"
end

# REMOVED: (feed not found)
# http://www.vogue.co.uk/spy/street-chic
# http://www.coggles.com/street-style
