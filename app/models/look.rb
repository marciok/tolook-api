class Look 
  include Mongoid::Document
  include Mongoid::Timestamps::Created

  after_create :flush_cache

  belongs_to :source
  has_and_belongs_to_many :occasions, index: true
  has_and_belongs_to_many :garments, index: true

  field :picture, type: String
  field :thumb, type: String
  field :reference, type: String

  index({ created_at: -1}, { unique: true })

  validates :picture, :thumb, :reference, presence: true
  validates :picture, uniqueness: true

  def self.cached_all
    Rails.cache.fetch('Look.all') { all }
  end

  def flush_cache
    Rails.cache.delete('Look.all')
  end

end
