class Garment
  include Mongoid::Document

  after_save :flush_cache

  field :name, type: String
  field :thumb, type: String
  field :entries, type: Integer, default: 0

  has_and_belongs_to_many :looks, index: true

  index({ name: 1}, { unique: true })

  validates :name, :thumb, presence: true
  validates :entries, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  def self.cached_all_by_name
    Rails.cache.fetch('Garment.all.order_by(name: 1)') { all.order_by(name: 1) }
  end

  def self.cached_find(id)
    Rails.cache.fetch([name, id]) { find(id) }
  end

  def flush_cache
    Rails.cache.delete('Garment.all.order_by(name: 1)')
    Rails.cache.delete([name, id])
  end
end
