class Category
  include Mongoid::Document

  has_and_belongs_to_many :sources

  field :name, type: String

  validates :name, presence: true
end
