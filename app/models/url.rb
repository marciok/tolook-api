class Url
  include Mongoid::Document

  embedded_in :source

  field :site, type: String
  field :feed, type: String

  validates :site, :feed, presence: true
end
