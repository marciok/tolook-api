class Source 
  include Mongoid::Document

  after_save :flush_cache

  has_and_belongs_to_many :categories
  has_many :looks
  embeds_one :url

  field :name, type: String
  field :title, type: String
  field :thumb, type: String
  field :entries, type: Integer, default: 0

  validates :name, :thumb, :title, presence: true
  validates :entries, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  def self.cached_all_by_title
    Rails.cache.fetch('Source.all.order_by(title: 1).gt(entries: 0)') { all.order_by(title: 1).gt(entries: 0) }
  end

  def self.cached_find(id)
    Rails.cache.fetch([name, id]) { find(id) }
  end

  def flush_cache
    Rails.cache.delete('Source.all.order_by(title: 1).gt(entries: 0)')
    Rails.cache.delete([self.class.name, id])
  end
end
