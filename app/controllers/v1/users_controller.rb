class V1::UsersController < V1Controller
  before_filter :find_user, except: :create

  class << self

    def inherited(subclass)

      subclass.instance_eval do
        define_method(:user_id_param) do
          params[:user_id]
        end

        protected :user_id_param
      end

      super
    end
  end

  def create
    @user = User.create!
    render :show, status: :created
  end

  protected

  def find_user
    @user = User.find(user_id_param)
  end

  def user_id_param
    params[:id]
  end

end
