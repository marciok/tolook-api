class V1::LooksController < V1Controller 

  def create
    @source = Source.where(title: params[:source_title]).first
    unless Look.where(picture: params[:picture]).first #TODO :REFACTOR THAT
      if @source
        @look = Look.create!({
          picture: params[:picture],
          thumb: params[:thumb],
          source: @source,
          reference: params[:url]
        })
        @look.garments =  (params[:garments] || []).collect do |garment|
          @garment = Garment.where(name: garment).first
          @garment.entries = @garment.entries + 1
          @garment.save!
          @garment
        end
        @look.occasions = (params[:occasions] || []).collect do |occasion|
          @occasion = Occasion.where(name: occasion).first
          @occasion.entries = @occasion.entries + 1
          @occasion.save!
          @occasion
        end
        if @look.save!
          @source.entries = @source.entries + 1
          @source.save!
          head :created
        else
          head :bad_request
        end
      end
      head :bad_request
    end
  end

  def index #TODO: Spec that <-
    if params[:garment_id]
      @garment = Garment.cached_find(params[:garment_id])
      looks = @garment.looks
    elsif params[:source_id]
      @source = Source.cached_find(params[:source_id])
      looks = @source.looks
    elsif params[:occasion_id]
      @occasion = Occasion.cached_find(params[:occasion_id])
      looks = @occasion.looks
    else
      looks = Look.cached_all
    end
    if params[:max_id]
      @looks = looks.where(:id.lt => params[:max_id]).limit(7).order_by(created_at: -1)
    else
      @looks = looks.limit(7).order_by(created_at: -1)
    end
  end

  def show
    @look = Look.find(params[:id])
  end

end

