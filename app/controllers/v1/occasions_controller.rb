class V1::OccasionsController < V1Controller

  def index
    @occasions = Occasion.cached_all_by_name
  end

end
