class V1::Users::FavoritesController < V1::UsersController

  def index
    @favorites = @user.favorites
  end

  def create
    @favorite = Look.find(params[:look_id])
    @user = User.find(params[:user_id])
    @user.favorites.push(@favorite)
    @user.save!
    head :created
  end

  def destroy #TODO spec that <-
    @favorite = Look.find(params[:id])
    if @favorite
      @user = User.find(params[:user_id])
      @user.favorites.delete(@favorite)
      head :ok
    end
  end

end

