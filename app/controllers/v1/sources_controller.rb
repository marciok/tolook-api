class V1::SourcesController < V1Controller

  def create
    @source = (Source.where(name: params[:source][:name]).first || Source.create!(params[:source]))
    @category = Category.where(name: params[:category_name]).first
    if @source
      @source.categories.push(@category) if @category
      head :created
    end
  end

  def index
    @sources = Source.cached_all_by_title
  end

end
