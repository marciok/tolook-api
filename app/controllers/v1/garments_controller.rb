class V1::GarmentsController < V1Controller

  def index
    @garments = Garment.cached_all_by_name
  end

end
