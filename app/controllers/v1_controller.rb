class V1Controller < ApplicationController
  include ActionController::MimeResponds

  # TODO: Test this filter
  around_filter :respond_json

  private

  # TODO: Test this method
  def respond_json
    respond_to do |format|
      format.json { yield }
    end
  end
end


