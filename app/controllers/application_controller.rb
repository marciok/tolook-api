require 'new_relic/agent/instrumentation/rails3/action_controller'

class ApplicationController < ActionController::API
  include NewRelic::Agent::Instrumentation::ControllerInstrumentation
  include NewRelic::Agent::Instrumentation::Rails3::ActionController

  rescue_from Mongoid::Errors::DocumentNotFound, with: :return_not_found

  private

  def return_not_found
    head :not_found
  end
end
